/**
 * system:visonary
 * app   :NextAction
 * create:2018.4.19 oota
 */
 /*★arg:フォルダ名★*/
 function createBoxFolda(event,folderFiled){
 	var clientCode="h45u4g3xpavyvprpb4oemev0skq31ar7";//クライアントコード
	var clientSecret="Lg4P7VHmiDbbVunzDYzPwr0yKhlbRmOn";//クライアント機密コード
	var token=getAccessToken(clientCode,clientSecret);
	var record=event.record;
	var folderName=record[folderFiled]["value"];//親フォルダ名
	var createFoldaList=[folderName,"履歴書","契約書","その他"];
	if(token){
		var header={
			'Content-Type': 'application/json',
			"Authorization": "Bearer " + token
		}
		//BOXにフォルダを作成する
		createFolda(header,createFoldaList,0);
	}
 }
 /**
  *function   : BOXにフォルダを生成する
  *@header    : 認証情報
  *@foldaName : 作成するフォルダの配列
  *@count     : 作成するフォルダ数
  *@foldaId   : 親フォルダID
  */
 function createFolda(header,folderName,count,foldaId){
 	 var id = foldaId || "48750374405";
	 var respId="";
	 var body={
		 name:folderName[count],
		 parent:{
			 id:id
		 }
	 }
	 return kintone.proxy('https://api.box.com/2.0/folders', 
				   'POST',header,body, function(body, status, headers) {
		 //success
		 console.log(JSON.parse(body));
		 //初回のみ親フォルダを変更する
		 if(count===0){
		 	var respId=JSON.parse(body).id;
		 }else{
		 	respId=foldaId;
		 }
		 if(count!==folderName.length-1){
		    count++;
		 	return createFolda(header,folderName,count,respId);
		 }else{
		 	return true;
		 }
	 }, function(error) {
		 //error
		 console.log(error);  //proxy APIのレスポンスボディ(文字列)を表示
	 });
}
/**
  *function       : Accessトークンを取得し、マスタを更新する
  *@clientCode    : クライアントID
  *@clientSecret  : シークレットID
  *return         : Accessトークン
  */
 function getAccessToken(clientCode,clientSecret){
	 var token=null;
	 //********************************
	 //マスタに格納してあるトークンを取得する
	 //********************************
	 var xmlHttp = new XMLHttpRequest();
	 var appUrl=kintone.api.url("/k/v1/record")+"?app="+117+'&id=1';
	 xmlHttp.open("GET", appUrl, false);
	 xmlHttp.setRequestHeader('X-Cybozu-API-Token', "4Jvm6tGkTBUGJKbANliQHgT4zuiHTlzjV5dg78uf");
	 xmlHttp.send(null);
	 if(xmlHttp.status === 200){
		 var resp=JSON.parse(xmlHttp.responseText);
		 var refresh_token=resp.record["refresh_token"]["value"];
		 //リフレッシュトークンを利用して、新しい認証トークンを取得する
		 var body={
			 grant_type:"refresh_token",
			 refresh_token:refresh_token,
			 client_id:clientCode,
			 client_secret:clientSecret
		 }
		 $.ajax({
			 type: 'POST',
			 url: 'https://api.box.com/oauth2/token',
			 data: body,
			 headers: {
				 'Content-Type': 'application/json',
			 },
			 async: false,
			 success: function(response) {
				 //新しく取得したトークンをマスタに上書きする
				 var putParam={
					 app:117,
					 id:resp.record["$id"]["value"],
					 record:{
						 refresh_token:{
							 value:response.refresh_token
						 },
						 access_token:{
							 value:response.access_token
						 }
					 },
					 __REQUEST_TOKEN__: kintone.getRequestToken()
				 }
				 var xhr = new XMLHttpRequest();
				 var url=kintone.api.url("/k/v1/record");
				 xhr.open('PUT', url,false);
				 xhr.setRequestHeader('X-Cybozu-API-Token', "4Jvm6tGkTBUGJKbANliQHgT4zuiHTlzjV5dg78uf");
				 xhr.setRequestHeader('Content-Type', 'application/json');
				 xhr.send(JSON.stringify(putParam));
				 if(xhr.status === 200){
					 token=response.access_token;
				 }else{
					 return null;
				 }
			 },
			 error: function(error) {
				 console.log(error);
			 }
		 });
	 }else{
		 return null;
	 }
	 return token;
 }